<?php get_header(); ?>


<div class="container">
    <div class= col-md-12 >
        <fieldset>
            <legend><h2>Você buscou por: <?php echo esc_html(get_query_var('s'));?></h2></legend>
            <div id="upload">
                <?php while( have_posts() ) : the_post();?>
                    <div class="col-md-12">
                        <a href="<?php echo the_permalink();?>" >
                            <div class="col-md-8">
                                <div class="col-md-8" >
                                    <div> <p><?php the_title(); ?></p> </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="btn btn-success">Acessar</div>
                            </div>
                        </a>
                    </div>
                <?php endwhile;?>
            </div>    
        </fieldset>
    </div>
</div>

<?php get_footer(); ?>