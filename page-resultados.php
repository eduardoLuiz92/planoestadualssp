<?php
    // Template Name: Resultados Avancados
?>

<?php get_header(); ?>

<?php
    $recursos = new WP_Query([
        // 'posts_per_page' => 3,
        'post_type'=>'resultadosavancados'
    ]);
?>

<div class="container">
    <div class="col-md-12">
        <fieldset>
            <legend><h2>Resultados Avançados</h2></legend>
            
            <div id="ultima_publicacao">
                <?php while ($recursos->have_posts()) : $recursos->the_post();?>
                    <?php if( $recursos->current_post == 0): ?>
                        <p> <?php echo the_content(); ?></p>
                    <?php endif;?>    
                <?php endwhile;?>
            </div>    
        </fieldset>
        <br>
        <fieldset>
            <legend><h2>Anteriores</h2></legend>
            <div id="upload">
                <?php while($recursos->have_posts()) : $recursos->the_post();?>
                    <div class="col-md-12">
                        <a href="<?php echo the_permalink();?>" >
                            <div class="col-md-8">
                                <div id="arquivo_indicadores_criminais_<?php echo get_the_id();?>" class="col-md-8" >
                                    <div> <p><?php the_title(); ?></p> </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-4">
                                <div class="btn btn-success">Acessar</div>
                            </div> -->
                        </a>
                    </div>
                <?php endwhile;?>
            </div>    
        </fieldset>
    </div>
</div>

<?php get_footer(); ?>