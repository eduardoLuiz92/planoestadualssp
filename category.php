<?php get_header(); ?>

<div class="container">

    <h3> arquivo generico categoria:  <?php echo single_cat_title();?></h3>

    <div class="col-md-6">
    <?php while( have_posts() ) : the_post(); ?>
            <div style="border: 1px solid black">
                <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                <?php if(is_single()): ?>
                    <p><?php the_content(); ?></p>
                <?php else: ?>
                    <p><?php the_excerpt(); ?></p>
                <?php endif; ?>
                <?php the_category(); ?>
                <?php echo the_permalink(); ?>
            </div>
        <?php endwhile;?>
    </div>
</div>

<?php get_footer(); ?>