
<?php 
    $query = new WP_Query([
        'post_per_page'=>'1',
        'category_name' => 'bannerindex'
    ]);
?>

<div id="home" class="container">
    <div class="col-md-12">
        <div id="banner_index" class="slidehome">
            <?php if ( function_exists( 'meteor_slideshow' ) ) { meteor_slideshow(); } ?>
        </div>
    </div>
</div>