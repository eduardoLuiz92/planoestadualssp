<div class="container">
    <div class="col-md-12">
        <fieldset>
            <legend><h2><?php the_title(); ?></h2></legend>
                <?php while( have_posts() ) : the_post(); ?>
                    <div id="post_unico">
                        <?php the_content(); ?>
                    </div>
                <?php endwhile;?>
        </fieldset>
    </div>
</div>