	<div>
		<footer>
			<div class="rodape">
				<!-- <img class="logo_planoestadual" src="<?php echo get_bloginfo('template_url');?>/img/plano estadual de seguranca publica.png"> -->
				<img class="logos_estado" src="<?php echo get_bloginfo('template_url');?>/img/logos-estado.png">
				<img class="logos_estado_icones" src="<?php echo get_bloginfo('template_url');?>/img/logos-estado-icones.png">
				<div class="fundo" ></div>
				<div class="copy col-md-12">
					<p>&copy; 2019 Plano estadual de segurança pública do Amazonas | Todos os Direitos Reservados. Construido por <a href="#">DST</a>.</p>
				</div>
			</div>			

			<script src="<?php echo get_bloginfo('template_url');?>/js/jquery.js" type="text/javascript"></script>
			<script src="<?php echo get_bloginfo('template_url');?>/js/bootstrap.min.js" type="text/javascript"></script>
			<script src="<?php echo get_bloginfo('template_url');?>/js/nav.js" type="text/javascript"></script>
			<script src="<?php echo get_bloginfo('template_url');?>/js/utils.js" type="text/javascript"></script>
		</footer>
	</div>
	<?php wp_footer();?>
</body>
</html>