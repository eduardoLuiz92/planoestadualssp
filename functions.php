<?php

add_action('init','planoestadual_action_init');

function planoestadual_action_init(){
    register_nav_menu('menu-principal','Menu principal (cabeçalho)');
    planoestadual_resultados_avancados_custom_post_type();
}

// adiciona post type de resultados avançados
function planoestadual_resultados_avancados_custom_post_type(){
    $descritivos = [
        'name' => 'Resultados Avançados',
        'singular_name' => 'Resultado Avançado',
        'add_new' => 'Adicionar Novo Resultado',
        'add_new_item' => 'Adicionar Resultado',
        'edit_item' => 'Editar Resultado',
        'new_item' => 'Novo R. Avançado',
        'view_item' => 'Ver Resultado',
        'search_items' => 'Buscar R. Avançado',
        'not_found' => 'Nenhum resultado avançado encontrado',
        'not_found_in_trash' => 'Sem Resultados na lixeira',
        'parent_item_colon' => '',
        'menu_name' => 'R. Avançados',
    ];

    $args =[
        'labels' => $descritivos,
        'public' => true,
        'hierarchical' => false,
        'menu_position'=> 5,
        'menu_icon' => 'dashicons-images-alt2',
        'supports' => [
            'title','editor','thumbnail','custom-fields','revisions'
        ]
    ];

    register_post_type('resultadosavancados',$args);
    flush_rewrite_rules();

}

// adiciona suporte a miniaturas
add_theme_support('post-thumbnails');

// adiciona tamanhos de img no wordpress
add_image_size('miniatura-resultados',150,135, false);
add_image_size('slide-home',458,254,true);

