
<?php get_header(); ?>

<div class="container">
    <div class="box">
        <div class="col-md-12">
            <fieldset>
                <legend><h2><?php echo single_cat_title();?></h2></legend>
                <div id="upload">
                    <?php while( have_posts() ) : the_post(); ?>
                        <div class="col-md-12">
                            <div id="arquivo_indicadores_criminais_<?php echo get_the_id();?>" class="col-md-8" >
                                <div> <?php the_content(); ?> </div>
                            </div>
                            <div class="col-md-4">
                                <div class="btn btn-success" onclick="baixar_indicador('<?php echo get_the_id();?>')">Baixar</div>
                            </div>
                        </div>
                    <?php endwhile;?>
                </div>
            </fieldset>
        </div>
    </div>
</div>

<?php get_footer(); ?>