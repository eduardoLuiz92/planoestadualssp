<div id="pesquisar">
    <form class="form-search-header" method="get" action="<?php bloginfo('home');?>">
        <input type="text" name="s" value="<?php get_search_query();?>" placeholder="O que você procura?" />
        <i class="fa fa-search"></i>
    </form>
</div>