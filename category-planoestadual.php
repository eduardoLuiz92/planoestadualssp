
<?php get_header(); ?>

<?php
    $query = new WP_Query([
        // 'posts_per_page' => 3,
        'category_name'=>'planoestadual'
    ]);
?>

<div class="container">
    <div class="col-md-12">
        <fieldset>
            <legend><h2>O Plano</h2></legend>
            
            <div id="ultima_publicacao">
                <?php while( $query->have_posts() ) : $query->the_post(); ?>
                    <?php if( $query->current_post == 0): ?>
                        <p> <?php the_content(); ?></p>
                    <?php endif;?>    
                <?php endwhile;?>
            </div>    
        </fieldset>
    </div>
</div>

<?php get_footer(); ?>