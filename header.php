<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title><?php bloginfo('name');?></title>
	<meta charset="<?php bloginfo('charset')?>">
	<meta name="description" content="<?php bloginfo('description')?>">
	<meta name="author" content="Equipe de desenvolvimento CICC/SEAGI/SSP-AM">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type= "text/css" href="<?php bloginfo('template_url');?>/css/style.css">
	<link rel="stylesheet" type= "text/css" href="<?php bloginfo('template_url');?>/css/nav.css">
	<link rel="stylesheet" type= "text/css" href="<?php bloginfo('template_url');?>/css/formulario.css">
	<link rel="shortcut icon" href="<?php echo get_bloginfo('template_url');?>/img/ssp.png" sizes="64x64" type="image/png">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head();?>
</head>

<body>

	<nav>
		
		<span id="brand">
			<a href="<?php echo get_bloginfo('home');?>"> <img src="<?php echo get_bloginfo('template_url');?>/img/plano estadual de seguranca publica.png"></a>
		</span>

		<?php 
			wp_nav_menu([
				'theme_location' => 'menu-principal',
				'container'=>'ul',
				'menu_class' => 'menu'
			]); 
		?>
		<!-- </div> -->

		<!-- instrução que inclui o arquivo do formulario de pesquisa no site -->
		<?php //get_search_form();?>

		<div id="toggle">
			<div class="span" id="one"></div>
			<div class="span" id="two"></div>
			<div class="span" id="three"></div>
		</div>
	</nav>

	<div id="resize">
		<?php 
			wp_nav_menu([
				'theme_location' => 'menu-principal',
				'container'=>'ul',
				'menu_class' => 'menu'
			]); 
		?>
	</div>

	<header>
		<script type="text/javascript">
			(function () {
			var options = {
			whatsapp: "+5592999999999", // Número do WhatsApp
			company_logo_url: "<?php bloginfo('url');?>/img/ssp.png", // URL com o logo da empresa
			greeting_message: "Olá! Contribua conosco falando das suas sugestões para o plano.", // Texto principal
			call_to_action: "Dê uma sugestão para o plano.", // Chamada para ação
			position: "right", // Posição do widget na página 'right' ou 'left'
			};
			var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
			var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
			s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
			var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
			})();
		</script>
	</header>


			